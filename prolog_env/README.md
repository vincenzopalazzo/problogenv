# ProbLogEnv

A docker image that use Python 3.7 and docker compose to run Prolog language with problog exstension

You can run the Prolog code with the follow command

```bash
docker-compose run -T problog_env problog hello.pl
```