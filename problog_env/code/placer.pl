placement(App, Placement) :-
	application(App, Services),
	servicePlacement(Services, Placement),
	flowOK(Placement).

servicePlacement(Services, Placement) :-
	servicePlacement(Services, [], _, [], Placement).

servicePlacement([], AllocHW, AllocHW, Placement, Placement).
servicePlacement([S|Ss], AllocHW, NewAllocHW, Placement, NewPlacement) :-
	oneServicePlacement(S, AllocHW, TempAllocHW, N),
	servicePlacement(Ss, TempAllocHW, NewAllocHW, [on(S,N)|Placement], NewPlacement).

oneServicePlacement(S, AllocHW, NewAllocHW, N) :-
	service(S, SWReqs, HWReqs, TReqs),
	node(N, SWCaps, HWCaps, TCaps),
	hwTh(T), HWCaps >= HWReqs + T,
	thingReqsOK(TReqs, TCaps),
	swReqsOK(SWReqs, SWCaps),
	hwReqsOK(HWReqs, HWCaps, N, AllocHW, NewAllocHW).

thingReqsOK(TReqs, TCaps) :- subset(TReqs, TCaps).

swReqsOK(SWReqs, SWCaps) :- subset(SWReqs, SWCaps).

hwReqsOK(HWReqs, HWCaps, N, [], [(N,HWReqs)]) :-
	hwTh(T), HWCaps >= HWReqs + T.
hwReqsOK(HWReqs, HWCaps, N, [(N,AllocHW)|L], [(N,NewAllocHW)|L]) :-
	NewAllocHW is AllocHW + HWReqs, hwTh(T), HWCaps >= NewAllocHW + T.
hwReqsOK(HWReqs, HWCaps, N, [(N1,AllocHW)|L], [(N1,AllocHW)|NewL]) :-
	N \== N1, hwReqsOK(HWReqs, HWCaps, N, L, NewL).

flowOK(P) :-
	findall(n2n(N1,N2,ReqLat,ReqBW), (member(on(S1,N1), P), member(on(S2,N2), P), N1 \== N2, s2s(S1, S2, ReqLat, ReqBW)), N2Ns),
	serviceFlowOK(N2Ns, [], _).

serviceFlowOK([], AllocBW, AllocBW).
serviceFlowOK([n2n(N1,N2,ReqLat,ReqBW)|Ss], AllocBW, NewAllocBW) :-
	link(N1, N2, FeatLat, FeatBW),
	FeatLat =< ReqLat,
	bwOK(N1, N2, ReqBW, FeatBW, AllocBW, TempAllocBW),
	serviceFlowOK(Ss, TempAllocBW, NewAllocBW).

bwOK(N1, N2, ReqBW, FeatBW, [], [(N1,N2,ReqBW)]):-
	bwTh(T), FeatBW >= ReqBW + T.
bwOK(N1, N2, ReqBW, FeatBW, [(N1,N2,AllocBW)|L], [(N1,N2,NewAllocBW)|L]):-
	NewAllocBW is ReqBW + AllocBW, bwTh(T), FeatBW >= NewAllocBW + T.
bwOK(N1, N2, ReqBW, FeatBW, [(N3,N4,AllocBW)|L], [(N3,N4,AllocBW)|NewL]):-
	\+ (N1 == N3, N2 == N4), bwOK(N1,N2,ReqBW,FeatBW,L,NewL).